TEMPLATE = app
CONFIG += console c++1z
CONFIG -= app_bundle
CONFIG -= qt
linux-g++ | linux-g++-64 | linux-g++-32 {
    QMAKE_CXX = g++-8
}
QMAKE_CXXFLAGS += -std=c++1z -O3
QMAKE_CXXFLAGS -= -std=gnu++11 -O2
#QMAKE_CXXFLAGS += -Wno-c++17-extensions
SOURCES += \
        main.cpp

HEADERS += \
    catch.hpp
