//#define CATCH_CONFIG_MAIN
//#include "catch.hpp"
#include <algorithm>
#include <functional>
#include <iostream>
#include <queue>
#include <tuple>
#include <vector>
using namespace std;

namespace SMD
{

class A
{
	int a, b;

public:
	A(int a, int b) : a(a), b(b)
	{
		a = b;
		std::cout << "Constructing\n";
	}
};
}

template <typename T>
auto f(T&& t) -> decltype(t)
{
	return t;
}
template <typename... T>
void print(T&&... t)
{
	((std::cout << t << ", "), ...);
	std::cout << std::endl;
	puts(__PRETTY_FUNCTION__);
}

template <typename C, typename... Args>
void push_back(C& c, Args&&... args)
{
	(c.push_back(args), ...);
	puts(__PRETTY_FUNCTION__);
}
template <class C, class T>
bool contains(const C& c, const T& value)
{
	std::cout << "got called with value : " << value << std::endl;
	return std::end(c) != std::find(std::begin(c), std::end(c), value);
}

template <typename C, typename... Args>
auto contains_any(C&& c, Args&&... args)
{
	return (contains(c, args) || ...);
}
int main()
{
	//	auto con_any = []<typename C, typename... Args>(C && c, Args && ... args)
	//	{
	//	};

	cout << "Hello World!" << endl;
	auto[i, j] = std::make_tuple(3, 3.2);
	decltype(j) k = 3;

	SMD::A a(3, 4);
	std::queue<std::function<void()>> q;

	auto l = [](int i) { std::cout << "hello\n"; };
	//	auto fn = ;
	std::invoke(l, 4);

	q.push(static_cast<std::function<void()>>([]() { std::cout << "hello\n"; }));
	q.push(static_cast<std::function<void()>>([]() { std::cout << "bye\n"; }));
	decltype(f(3.2)) x = 3;

	while (!q.empty())
	{

		(q.front())();
		q.pop();
	}
	int&& o = 433;
	print(3, i, j, k, o);
	print(i, j);
	std::vector<int> v{ 3, 4, 2 };
	push_back(v, 34, i, o);
	push_back(v);
	for (const auto& i : v)
		std::cout << i << " ";
	std::cout << '\n';

	std::cout << contains_any(v, -3, 44, -5, 34, -3) << std::endl; // true
	std::cout << contains_any("I dream and the world trembles", 'f', 'x', 'd') << std::endl; // true

	std::cout << contains_any(std::vector{ 9, 9, 9, 8 }, -3, 44, -5, 34, -3) << std::endl; // false
	// checking stuff
	return 0;
}
